import React from 'react';
import ContentEditable from 'react-contenteditable'
import './App.css';
import './index.css';
import Leaflet from 'leaflet';
import { Map, Marker, TileLayer, Rectangle } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';

var Maidenhead = require('maidenhead');


const markerIcon = Leaflet.icon({
    iconSize: [25, 41],
    iconAnchor: [10, 41],
    popupAnchor: [2, -40],
    iconUrl: "https://unpkg.com/leaflet@1.5.1/dist/images/marker-icon.png",
    shadowUrl: "https://unpkg.com/leaflet@1.5.1/dist/images/marker-shadow.png"
});

function createLoc(lat, lon, prec)  {
  let loc3 = new Maidenhead(lat, lon, 3);
  let loc6 = new Maidenhead(lat, lon, 6);
  let loc = {locator: loc3.locator, lat: loc6.lat, lon: loc6.lon}
  return loc;
}



class HamMap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {zoom: 12}
  }
    render() {
      let position = [this.props.lat, this.props.lon]
      var squareCenter = Maidenhead.toLatLon(createLoc(position[0], position[1], 6).locator)
          return (
            <Map
              center={position}
              zoom={this.state.zoom}
              style={{width: this.props.width, height: this.props.height}}>
                <TileLayer
                attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              />
                <Marker position={position} icon={markerIcon}>
                </Marker>
                <Rectangle
                  bounds={[[parseFloat(squareCenter[0])-2.5/120, parseFloat(squareCenter[1])-5/120],[parseFloat(squareCenter[0])+2.5/120,parseFloat(squareCenter[1])+5/120]]}
                  color='red'
                />
              </Map>
            )
        }
}


class LocationBox extends React.Component {
  constructor(props) {
    super(props);
    this.contentEditableLocator = React.createRef();
    this.contentEditableNotes = React.createRef();
    this.contentEditableLat = React.createRef();
    this.contentEditableLon = React.createRef();
    this.state = {
      loc: {},
      locatorHTML: "",
      latHTML: "",
      lonHTML: "",
      notes: "Notes...",
    }
  }
  handleNotesChange = evt => {
    this.setState({notes: evt.target.value});
  };
  handleLocatorChange = evt => {
    this.setState({locatorHTML: evt.target.value});
    try {
      let coords = Maidenhead.toLatLon(evt.target.value);

      let loc = createLoc(coords[0], coords[1], 6);
      this.setState({loc: loc})
      this.setState({
        loc: loc,
        latHTML: loc.lat.toString(),
        lonHTML: loc.lon.toString(),
      });
    } catch(error) {
      console.log(error);
    }
  };
  handleLatChange = evt => {
    this.setState({latHTML: evt.target.value});
    try {
      let loc = createLoc(evt.target.value, this.state.loc.lon, 6);
      this.setState({
        loc: loc,
        locatorHTML: loc.locator,
        lonHTML: loc.lon.toString(),
      });
    } catch(error) {
      console.log(error);
    }
  };
  handleLonChange = evt => {
    this.setState({lonHTML: evt.target.value});
    try {
      let loc = createLoc(this.state.loc.lat, evt.target.value, 6);
      this.setState({
        loc: loc,
        locatorHTML: loc.locator,
        latHTML: loc.lat.toString(),
      });
    } catch(error) {
      console.log(error);
    }
  };
  componentDidMount() {
    this.getLocation();
  }
  setLocation(pos) {
    let loc = createLoc(pos.coords.latitude, pos.coords.longitude, 6);
    this.setState({
      loc: loc,
      locatorHTML: loc.locator,
      latHTML: loc.lat.toString(),
      lonHTML: loc.lon.toString(),
    });
  }
  getLocation() {
    this.setLocation({coords: {latitude: 51.9976276, longitude: -0.742304}})
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((pos) => this.setLocation(pos));
    }
  }
  render() {
    return (
      <div className="location-area">
        <div className="lat">
        <ContentEditable
          innerRef={this.contentEditableLat}
          html={this.state.latHTML} // innerHTML of the editable div
          disabled={false}       // use true to disable editing
          onChange={this.handleLatChange} // handle innerHTML change
          tagName='span' // Use a custom HTML tag (uses a div by default)
        />
        </div>
        <div className="lon">
        <ContentEditable
          innerRef={this.contentEditableLon}
          html={this.state.lonHTML} // innerHTML of the editable div
          disabled={false}       // use true to disable editing
          onChange={this.handleLonChange} // handle innerHTML change
          tagName='span' // Use a custom HTML tag (uses a div by default)
        />
        </div>
        <div className="locator">
        <ContentEditable
          innerRef={this.contentEditableLocator}
          html={this.state.locatorHTML} // innerHTML of the editable div
          onChange={this.handleLocatorChange} // handle innerHTML change
          tagName='span' // Use a custom HTML tag (uses a div by default)
        />
        </div>
        <div className="map">
      <HamMap
        lon={this.state.lonHTML}
      lat={this.state.latHTML}
      />
      </div>
      <div className="notes">
        <ContentEditable
          innerRef={this.contentEditableNotes}
          html={this.state.notes} // innerHTML of the editable div
          onChange={this.handleNotesChange} // handle innerHTML change
        />
      </div>
      </div>
    );
  }
}


class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {date: this.getTime()}

  }
  componentDidMount() {
    this.timeID = setInterval(
      () => this.tick(),
      1000
    );
  }
  componentWillUnmount() {
    clearInterval(this.timerID);
  }
  getTime() {
    return new Date().toLocaleTimeString('en-GB')
  }
  tick() {
    this.setState({
      date: this.getTime()
    });
  }
  render() {
    return (
      <span>{this.state.date}</span>
    );
  }
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {loc: {}}
  }
  render() {
    return (
      <div>
        <div className="clock">
        <Clock />
      </div>
        <LocationBox />
      </div>
    );
  }
}

export default App;
